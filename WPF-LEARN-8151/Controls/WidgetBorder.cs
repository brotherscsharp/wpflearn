﻿using System.Windows;
using System.Windows.Controls;

namespace WPF_LEARN_8151.Controls
{
    public class WidgetBorder : Control
    {
        static WidgetBorder() => DefaultStyleKeyProperty.OverrideMetadata(typeof(WidgetBorder), new FrameworkPropertyMetadata(typeof(WidgetBorder)));

        #region CornerRadius

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius), typeof(CornerRadius), typeof(WidgetBorder), new PropertyMetadata(default(CornerRadius)));

        public CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        #endregion

        #region BackgroundOpacity

        public static readonly DependencyProperty BackgroundOpacityProperty = DependencyProperty.Register(
            nameof(BackgroundOpacity), typeof(double), typeof(WidgetBorder), new PropertyMetadata(default(double)));

        public double BackgroundOpacity
        {
            get => (double)GetValue(BackgroundOpacityProperty);
            set => SetValue(BackgroundOpacityProperty, value);
        }

        #endregion
    }
}
