﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPF_LEARN_8151.Controls
{
    
    public class ButtonPanel : Button
    {
        static ButtonPanel() => DefaultStyleKeyProperty.OverrideMetadata(typeof(ButtonPanel), new FrameworkPropertyMetadata(typeof(ButtonPanel)));

        #region CornerRadius

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius), typeof(CornerRadius), typeof(ButtonPanel), new PropertyMetadata(default(CornerRadius)));

        public CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        #endregion

        #region OverBorderThickness

        public static readonly DependencyProperty OverBorderThicknessProperty = DependencyProperty.Register(
            nameof(OverBorderThickness), typeof(Thickness), typeof(ButtonPanel), new PropertyMetadata(default(Thickness)));

        public Thickness OverBorderThickness
        {
            get => (Thickness)GetValue(OverBorderThicknessProperty);
            set => SetValue(OverBorderThicknessProperty, value);
        }

        #endregion

        #region OverBorderBrush

        public static readonly DependencyProperty OverBorderBrushProperty = DependencyProperty.Register(
            nameof(OverBorderBrush), typeof(Brush), typeof(ButtonPanel), new PropertyMetadata(default(Brush)));

        public Brush OverBorderBrush
        {
            get => (Brush)GetValue(OverBorderBrushProperty);
            set => SetValue(OverBorderBrushProperty, value);
        }

        #endregion
    }
}
