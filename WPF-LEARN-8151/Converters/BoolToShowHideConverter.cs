﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace WPF_LEARN_8151.Converters
{
    public class BoolToShowHideConverter : MarkupExtension, IValueConverter
    {
        public BoolToShowHideConverter()
        {
            Inverse = false;
        }
        public BoolToShowHideConverter(bool isInverse)
        {
            Inverse = isInverse;
        }

        public bool Inverse { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value ^ Inverse) ? "Hide" : "Show";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) => throw new NotImplementedException();

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            /*
            if (_converter == null)
                _converter = new BoolToShowHideConverter(Inverse);
            return _converter;
            */
            return new BoolToShowHideConverter(Inverse);
        }

        // private static BoolToShowHideConverter _converter = null;
    }
}
