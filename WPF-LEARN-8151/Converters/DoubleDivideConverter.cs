﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace WPF_LEARN_8151.Converters
{
    public class DoubleDivideConverter : MarkupExtension, IValueConverter
    {
        private DoubleDivideConverter _converter;
        public DoubleDivideConverter()
        {

        }

        public double Divider { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            (double?)value / Divider ?? 0;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null)
                _converter = new DoubleDivideConverter();
            return _converter;
        }
    }
}
