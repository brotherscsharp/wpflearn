﻿using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WPF_LEARN_8151.Classes;

namespace WPF_LEARN_8151.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        private IRegionManager _rm;
        public MainWindowViewModel(IRegionManager regionManager, IEventAggregator eventAggregator) : base(regionManager, eventAggregator)
        {
            _rm = regionManager;
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
        }

        private RelayCommand _pageLoadedCommang;
        public RelayCommand PageLoadedCommang
        {
            get
            {
                return _pageLoadedCommang ??
                  (_pageLoadedCommang = new RelayCommand(obj =>
                  {
                      RegionManager.RequestNavigate(RegionNames.VisibilityTab, ViewNames.VisibilityScreen);
                      RegionManager.RequestNavigate(RegionNames.PrismTab, ViewNames.MainScreen);
                      RegionManager.RequestNavigate(RegionNames.StylesTab, ViewNames.StylesScreen);
                  }));
            }
        }

        private string _title = "WPF Test app";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
    }
}
