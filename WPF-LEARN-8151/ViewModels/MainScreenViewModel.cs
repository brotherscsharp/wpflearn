﻿using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using WPF_LEARN_8151.Classes;

namespace WPF_LEARN_8151.ViewModels
{
    public class MainScreenViewModel : BaseViewModel
    {
        private readonly IDialogService _dialogService;
        public MainScreenViewModel(IRegionManager regionManager, IEventAggregator eventAggregator, IDialogService dialogService) : base(regionManager, eventAggregator)
        {
            _dialogService = dialogService;
            EventAggregator.GetEvent<TickerSymbolSelectedEvent>().Subscribe(EventAggregatorShowTestMessage, ThreadOption.UIThread);
        }

        
        private string _eventAggregatorText;
        public string EventAggregatorText
        {
            get => _eventAggregatorText;
            set => SetProperty(ref _eventAggregatorText, value);
        }

        private string _dialogServiceResult;
        public string DialogServiceResult
        {
            get => _dialogServiceResult;
            set => SetProperty(ref _dialogServiceResult, value);
        }        

        private DelegateCommand _вialogServiceCommand = null;
        public DelegateCommand DialogServiceCommand => _вialogServiceCommand ??= new DelegateCommand(DialogServiceTest);


        private DelegateCommand _eventAggregatorCommand = null;
        public DelegateCommand EventAggregatorCommand => _eventAggregatorCommand ??= new DelegateCommand(EventAggregatorTest);
        private DelegateCommand _regionTestViewOneCommand = null;
        public DelegateCommand RegionTestViewOneCommand => _regionTestViewOneCommand ??= new DelegateCommand(RegionTestViewOne);


        private DelegateCommand _regionTestViewTwoCommand = null;
        public DelegateCommand RegionTestViewTwoCommand => _regionTestViewTwoCommand ??= new DelegateCommand(RegionTestViewTwo);

        private void DialogServiceTest()
        {
            var message = "This is a message that should be shown in the dialog.";
            //using the dialog service as-is
            _dialogService.ShowDialog("NotificationDialog", new DialogParameters($"message={message}"), r =>
            {
                if (r.Result == ButtonResult.None)
                    DialogServiceResult = "Result is None";
                else if (r.Result == ButtonResult.OK)
                    DialogServiceResult = "Result is OK";
                else if (r.Result == ButtonResult.Cancel)
                    DialogServiceResult = "Result is Cancel";
                else
                    DialogServiceResult = "I Don't know what you did!?";
            });
        }

        private void RegionTestViewOne()
        {
            RegionManager.RequestNavigate(RegionNames.RegionsTest, ViewNames.ViewOne);
        }
        private void RegionTestViewTwo()
        {
            var navigationParameters = new NavigationParameters();
            navigationParameters.Add("message", "Message from parent");
            RegionManager.RequestNavigate(RegionNames.RegionsTest, ViewNames.ViewTwo, navigationParameters);
        }

        private void EventAggregatorTest()
        {
            EventAggregator.GetEvent<TickerSymbolSelectedEvent>().Publish(EventAggregatorText);
        }

        private void EventAggregatorShowTestMessage(string message)
        {
            MessageBox.Show((message == null || message.Length == 0) ? "please type text" : message);
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

        }
    }
}
