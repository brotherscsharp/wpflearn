﻿using Prism.Events;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_LEARN_8151.ViewModels
{
    public class RegionTestViewTwoViewModel : BaseViewModel
    {
        public RegionTestViewTwoViewModel(IRegionManager regionManager, IEventAggregator eventAggregator) : base(regionManager, eventAggregator)
        {
        }
        private string _messageFromParent;
        public string MessageFromParent
        {
            get => _messageFromParent;
            set => SetProperty(ref _messageFromParent, value);
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            MessageFromParent = navigationContext.Parameters.GetValue<string>("message");

        }
    }
}
