﻿using Prism.Events;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_LEARN_8151.ViewModels
{
    public class StylesScreenViewModel : BaseViewModel
    {
        public StylesScreenViewModel(IRegionManager regionManager, IEventAggregator eventAggregator) : base(regionManager, eventAggregator)
        {
            IsBusy = true;
            TestText = "Test text";
        }

        private string _testText;
        public string TestText
        {
            get => _testText;
            set => SetProperty(ref _testText, value);
        }

        
    }
}
