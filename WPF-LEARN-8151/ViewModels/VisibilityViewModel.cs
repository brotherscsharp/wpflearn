﻿using Prism.Events;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WPF_LEARN_8151.Classes;

namespace WPF_LEARN_8151.ViewModels
{
    public class VisibilityViewModel : BaseViewModel
    {
        public VisibilityViewModel(IRegionManager regionManager, IEventAggregator eventAggregator) : base(regionManager, eventAggregator)
        {

            HasInstalled = true;
        }

        private RelayCommand showHideCommand;
        public RelayCommand ShowHideCommand
        {
            get
            {
                return showHideCommand ??
                  (showHideCommand = new RelayCommand(obj =>
                  {
                      HasInstalled = !HasInstalled;
                  }));
            }
        }

        private void ShowOrHideLabel(object target, ExecutedRoutedEventArgs e)
        {
            HasInstalled = !HasInstalled;
        }

        private bool _hasInstalled;
        public bool HasInstalled
        {
            get { return _hasInstalled; }
            set { SetProperty(ref _hasInstalled, value); }
        }
    }
}
