﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_LEARN_8151.Interfaces
{
    public interface ICustomerStore
    {
        List<string> GetAll();
    }
}
