﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_LEARN_8151.Interfaces
{
    public interface ICommand
    {
        event EventHandler CanExecuteChanged;
        void Execute(object parameter);
        bool CanExecute(object parameter);
    }
}
