﻿using Prism.Ioc;
using Prism.Unity;
using System.Windows;
using WPF_LEARN_8151.Classes;
using WPF_LEARN_8151.Interfaces;
using WPF_LEARN_8151.ViewModels;
using WPF_LEARN_8151.Views;

namespace WPF_LEARN_8151
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {

        protected override Window CreateShell() => Container.Resolve<MainWindow>();

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<ICustomerStore, DbCustomerStore>();
            // register other needed services here
            containerRegistry.RegisterDialog<NotificationDialog, NotificationDialogViewModel>();

            RegisterViews(containerRegistry);
        }
        private static void RegisterViews(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<MainScreenView>(ViewNames.MainScreen);
            containerRegistry.RegisterForNavigation<VisibilityView>(ViewNames.VisibilityScreen);
            containerRegistry.RegisterForNavigation<RegionTestViewOne>(ViewNames.ViewOne);
            containerRegistry.RegisterForNavigation<RegionTestViewTwo>(ViewNames.ViewTwo);
            containerRegistry.RegisterForNavigation<StylesScreenView>(ViewNames.StylesScreen);
            
        }
    }
}
