﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF_LEARN_8151.Classes
{
    public class AsyncCommand<TParam> : ICommand
    {
        public event EventHandler CanExecuteChanged = delegate { };

        private readonly Func<TParam, Task> _execute;
        private readonly Func<TParam, bool>? _canExecute;
        private readonly Action? _callerChangeStateAction;
        private readonly Func<Exception, string> _errorMessageFunc;
        private readonly SynchronizationContext? _synchronizationContext;
        private bool _isExecuting;

        public AsyncCommand(Func<TParam, Task> execute, Func<TParam, bool>? canExecute = null, Action? callerChangeStateAction = null,
            Func<Exception, string>? errorMessageFunc = null)
        {
            _execute = execute;
            _canExecute = canExecute;
            _callerChangeStateAction = callerChangeStateAction;
            _errorMessageFunc = errorMessageFunc ?? (_ => "Operation cannot be completed due to critical error.");
            _synchronizationContext = SynchronizationContext.Current;
        }

        public bool CanExecute(TParam parameter) => !IsExecuting && (_canExecute?.Invoke(parameter) ?? true);

        public bool IsExecuting
        {
            get => _isExecuting;
            set
            {
                _isExecuting = value;
                _callerChangeStateAction?.Invoke();
            }
        }

        public async Task ExecuteAsync(TParam param)
        {
            if (CanExecute(param))
            {
                try
                {
                    IsExecuting = true;
                    RaiseCanExecuteChanged();

                    try
                    {
                        await _execute(param);
                    }
                    catch (OperationCanceledException ex)
                    {
                        // _log.Information("Task has been cancelled {@Exception}", ex);
                    }
                    catch (Exception ex)
                    {
                        // _log.Error(ex, "Unhandled command exception");
                        // _dialogsService.ShowError(_errorMessageFunc(ex), "Something went wrong");
                    }
                }
                finally
                {
                    IsExecuting = false;
                    RaiseCanExecuteChanged();
                }
            }
            else
            {
                RaiseCanExecuteChanged();
            }
        }

        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                if (_synchronizationContext != null && _synchronizationContext != SynchronizationContext.Current)
                    _synchronizationContext.Post((o) => handler.Invoke(this, EventArgs.Empty), null);
                else
                    handler.Invoke(this, EventArgs.Empty);
            }
        }

        #region Explicit implementations

        bool ICommand.CanExecute(object parameter) => CanExecute((TParam)parameter);

        void ICommand.Execute(object parameter)
        {
            _ = ExecuteAsync((TParam)parameter);// .CatchExceptions('');
        }

        #endregion
    }

    public class AsyncCommand : AsyncCommand<object>
    {
        public AsyncCommand(Func<Task> execute, Func<bool>? canExecute = null, Action? callerChangeStateAction = null,
            Func<Exception, string>? errorMessageFunc = null)
            : base(_ => execute(), canExecute == null ? (Func<object, bool>?)null : _ => canExecute!(), callerChangeStateAction, errorMessageFunc)
        {
        }

        public bool CanExecute() => base.CanExecute(default!);

        public Task ExecuteAsync() => base.ExecuteAsync(default!);
    }
}