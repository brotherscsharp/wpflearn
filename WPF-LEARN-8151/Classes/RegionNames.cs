﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_LEARN_8151.Classes
{
    public static class RegionNames
    {
        public const string MainContent = nameof(MainContent);
        public const string VisibilityTab = nameof(VisibilityTab);
        public const string PrismTab = nameof(PrismTab);
        public const string StylesTab = nameof(StylesTab);
        public const string RegionsTest = nameof(RegionsTest);        
        
    }
}
