﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_LEARN_8151.Views;

namespace WPF_LEARN_8151.Classes
{
    public static class ViewNames
    {
        public const string MainScreen = nameof(MainScreenView);
        public const string VisibilityScreen = nameof(VisibilityView);
        public const string prismScreen = nameof(MainScreenView);
        public const string ViewOne = nameof(RegionTestViewOne);
        public const string ViewTwo = nameof(RegionTestViewTwo);
        public const string StylesScreen = nameof(StylesScreenView);
        
    }
}
