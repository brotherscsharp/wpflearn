﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_LEARN_8151.Classes
{
    public class TickerSymbolSelectedEvent : PubSubEvent<string> { }
}
